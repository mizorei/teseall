<?xml version="1.0" encoding="UTF-8"?>
<!--
 Provides a xacro macro to insert the cambada robot arm.
 NOTE: The gazebo plugins to control the arm are *not* loaded
 from within this file, in order to avoid dependencies to other
 packages. You will have to add the <gazebo> tags to load the
 model plugins from where you call this xacro macro.
 This file however *can* include the <transmission> tags
 for the joints (include file "$(find cambada_description)/urdf/cambada_joint_control.xacro")
 and/or "$(find cambada_description)/urdf/cambada_joint_control_vel.xacro")
 according to arguments add_transmission_pos and add_transmission_vel.
-->
<root 
 xmlns:sensor="http://playerstage.sourceforge.net/gazebo/xmlschema/#sensor"
 xmlns:controller="http://playerstage.sourceforge.net/gazebo/xmlschema/#controller"
 xmlns:interface="http://playerstage.sourceforge.net/gazebo/xmlschema/#interface"
 xmlns:xacro="http://ros.org/wiki/xacro">

    <!-- add transmission tags for position controllers -->
    <xacro:arg name="add_transmission_pos" default="false"/>
    <!-- add transmission tags for velocity controllers -->
    <xacro:arg name="add_transmission_vel" default="false"/>

	<xacro:macro name="default_material">
      <!--  <mu1>10</mu1>
        <mu2>10</mu2>
	 	<kp>1000.0</kp>
	 	<kd>1.0</kd>  -->
	</xacro:macro>

    <!-- IMPORTANT NOTE about arm and finger limits: These values are optimized for mass dividers between 2 (for fingers 1-3)
         for using gazebo_ros_control hardware interface controllers.
         The seemingly most important factor in making robots explode is too much force allowed. Higher PID gains don't have too
         much of an effect. Should maybe start testing with higher PID values to challenge hitting the max force.
         The mass also plays a role. The lighter the robot, the easier it explodes when movements are made.
         When it is heavy, it is harder to lift the arm, and when it moves down, there's a risk of it collapsing down and then exploding after.
         For fingers: with current values, it works only with starting from 1.1 - 1.0 doesn't move. When it's too high, the robot explodes (last tested with finger mass div 1). 
         Velocity was tested between 0.1 and 0.3 but not sure it has an effect.  
         UPDATE 30.1: reducing the damping and friction for some joints has greatly improved the outcome. Damping/Friction probably also has played a roll in the robot collapses. -->
	<!-- Note for reading this in gazebo model (2015): effort and velocity limits are not yet imported properly in gazebo. 
         Update the driver after it does! Now it's hard-coded in C Macros -->
	<xacro:macro name="default_finger_limits">
		<limit lower="-0.0045" upper="1.05" effort="1.5" velocity="0.5" />
	</xacro:macro>

	<xacro:macro name="arm_0_limits">
		<limit effort="8" velocity="1.0" />
	</xacro:macro>
	
<xacro:macro name="joint_1">
		<limit lower="-1.5" upper="1.5" effort="8" velocity="0.5" />
	</xacro:macro>
	<xacro:macro name="joint_2">
		<limit lower="-1.5" upper="1.5" effort="8" velocity="0.5" />
	</xacro:macro>
<xacro:macro name="joint_3">
		<limit lower="-1.5" upper="1.5" effort="6" velocity="0.5" />
	</xacro:macro>
<xacro:macro name="joint_4">
		<limit lower="-1.5" upper="1.5" effort="6" velocity="0.5" />
	</xacro:macro>
	<xacro:macro name="joint_5">
		<limit lower="-1.5" upper="1.5" effort="6" velocity="0.5" />
	</xacro:macro>
<xacro:macro name="joint_6">
		<limit lower="-1.5" upper="1.5" effort="6" velocity="0.5" />
	</xacro:macro>


	<xacro:macro name="arm_1_limits" params="l u">
		<limit lower="${l}" upper="${u}" effort="8" velocity="1.0" />
	</xacro:macro>
	
    <xacro:macro name="arm_2_limits" params="l u">
		<limit lower="${l}" upper="${u}" effort="4" velocity="1.0" />
	</xacro:macro>
	
	<xacro:macro name="arm_3_limits">
		<limit effort="1.5" velocity="1.0" />
	</xacro:macro>

	<xacro:macro name="arm_4_limits">
		<limit effort="1.0" velocity="1.0" />
	</xacro:macro>
	
	<!--- remark 2014: damping greater than 0.5 lead to model break downs when testing it bit by bit -->
    <xacro:macro name="default_damping_low">
		<dynamics damping="0.05" friction="0.05"/>
	</xacro:macro>

	<xacro:macro name="default_damping_medium">
		<dynamics damping="0.5" friction="0.5"/>
	</xacro:macro>

	<xacro:macro name="default_damping_high">
		<dynamics damping="0.7" friction="1.0"/>
	</xacro:macro>

	<xacro:macro name="damping_fingers">
		<!--- remark 2014: damping greater than 0.1 (0.2) lead to model break downs when testing it bit by bit -->
        <!--- remark jan 2016: damping and friction higher than 0.005 required very high forces in order for the fingers
              to move at all, and then they mooved only slowly. -->
		<!--dynamics damping="0.1" friction="1.0"/-->
		<dynamics damping="0.005" friction="0.005"/>
	</xacro:macro>
	 
	<!-- Formula according to https://en.wikipedia.org/wiki/List_of_moment_of_inertia_tensors for cylinder -->
	<xacro:macro name="cyl_inertia" params="mass r h ">
		<mass value="${mass}"/>
		<inertia ixx="${0.083333 * mass * (3*r*r + h*h)}" ixy="0" ixz="0" 
                 iyy="${0.083333 * mass * (3*r*r + h*h)}" iyz="0" 
                 izz="${0.5*mass*r*r}" />
	</xacro:macro>

	<xacro:macro name="testSphere">
	</xacro:macro>
	
	<xacro:property name="M_PI" value="3.14159265358979" /> 
	<xacro:property name="M_PI_2" value="1.5707963267949" /> 
	<xacro:property name="M_PI_4" value="0.785398163" /> 
	

	<!-- ***************** MODEL CORRECTIONS ************************* -->	

	<!-- MODEL_FIX corrections for hand limb, which is not modelled correctly. 
	<xacro:property name="wrist_scale_x" value="1.12"/>
	<xacro:property name="wrist_scale_y" value="1.05"/>
	<xacro:property name="wrist_scale_z" value="1.05"/>
	-->
	<xacro:property name="wrist_scale_x" value="1.0"/>
	<xacro:property name="wrist_scale_y" value="1.0"/>
	<xacro:property name="wrist_scale_z" value="1.0"/>

	<!-- MODEL_FIX transform of base to first joint as derived from DH specs is not exact, this is a correction 
	<xacro:property name="base_height_corr" value="-0.005"/>
	<xacro:property name="base_height_corr" value="-0.005"/>
-->
	<xacro:property name="base_height_corr" value="0"/>
	<xacro:property name="base_height_corr" value="0"/>
	

	<!-- MODEL_FIX finger tip visuals are not long enough -->	
	<!--
	<xacro:property name="tip_scale_x" value="1.17"/>
	<xacro:property name="tip_scale_y" value="1"/>
	<xacro:property name="tip_scale_z" value="1"/>
	-->
	<xacro:property name="tip_scale_x" value="1"/>
	<xacro:property name="tip_scale_y" value="1"/>
	<xacro:property name="tip_scale_z" value="1"/>
	
	<!-- *************************************************************** -->	

	<!-- MODEL_FIX transforms from wrist to finger joints. Measures from the kinova spec image (Hand.png), pitch angles calculated from measures and experimentally adapted -->
	<!--<xacro:property name="f_thb_xyz" value=" 0.0319  0.003  0.1214" />
	<xacro:property name="f_idx_xyz" value="-0.0276  0.0204 0.1214" />
	<xacro:property name="f_pnk_xyz" value="-0.0276 -0.0204 0.1214" /> -->
	<xacro:property name="f_thb_xyz" value=" 0.0319  0.003  0.11" />
	<xacro:property name="f_idx_xyz" value="-0.0276  0.0204 0.11" />
	<xacro:property name="f_pnk_xyz" value="-0.0276 -0.0204 0.11" />

	<xacro:property name="f_thb_rpy" value="0 -0.37 ${M_PI-0.226892}" /> <!-- from measurements, pitch is -0.2658 --> 
	<xacro:property name="f_idx_rpy" value="0 -0.34 -0.191986" /> <!-- from measurements, pitch is -0.2293 -->
	<xacro:property name="f_pnk_rpy" value="0 -0.34 0.191986" /> <!-- from measurements, pitch is -0.2293 -->

	<!-- MODEL_FIX transforms from wrist to finger mounts -->
	<!--<xacro:property name="f_thb_m_xyz" value=" 0.037  0.001  0.106" />
	<xacro:property name="f_idx_m_xyz" value="-0.034  0.022 0.106" />
	<xacro:property name="f_pnk_m_xyz" value="-0.034 -0.022 0.106" />
	<xacro:property name="f_thb_m_rpy" value="0 -0.2658 ${M_PI-0.226892}" /> 
	<xacro:property name="f_idx_m_rpy" value="0 -0.2293 -0.191986" />
	<xacro:property name="f_pnk_m_rpy" value="0 -0.2293 0.191986" />-->

	<xacro:property name="f_thb_m_xyz" value=" 0.037  0.001  0.095" />
	<xacro:property name="f_idx_m_xyz" value="-0.034  0.022 0.095" />
	<xacro:property name="f_pnk_m_xyz" value="-0.034 -0.022 0.095" />
	<xacro:property name="f_thb_m_rpy" value="0 -0.2658 ${M_PI-0.226892}" /> 
	<xacro:property name="f_idx_m_rpy" value="0 -0.2293 -0.191986" />
	<xacro:property name="f_pnk_m_rpy" value="0 -0.2293 0.191986" />
	
	

	<!-- MACRO FOR THE ROBOT ARM -->
	<xacro:macro name="cambada_arm" params="parent mass_divider finger_mass_divider *origin cambada_prefix:=cambada">

    <joint name="${cambada_prefix}_arm_joint" type="fixed">
		<xacro:insert_block name="origin" />
		<parent link="${parent}" default="world" />
        <child link="${cambada_prefix}_0_baseA" />
	</joint>

    <gazebo reference="${cambada_prefix}_ring_">
		<material>Gazebo/Grey</material>
		
		<xacro:default_material/>
	</gazebo>
	<gazebo reference="_limb">
		<material>Gazebo/FlatBlack</material>
		<implicitSpringDamper>1</implicitSpringDamper>
		<xacro:default_material/>
	</gazebo>
	<gazebo reference="_finger">
		<material>Gazebo/Grey</material>
		<implicitSpringDamper>1</implicitSpringDamper>
		<xacro:default_material/>
	</gazebo>

	<!-- for some reason, material only applies if full name specified -->
    <gazebo reference="${cambada_prefix}_finger2">
		<xacro:default_material/>
	</gazebo>
    <gazebo reference="${cambada_prefix}_finger1">
		<xacro:default_material/>
	</gazebo>

    <gazebo reference="${cambada_prefix}_fing1">
		<provideFeedback value="true"/>
	</gazebo>
    <gazebo reference="${cambada_prefix}_fing2">
		<provideFeedback value="true"/>
	</gazebo>
 

    <xacro:if value="$(arg add_transmission_pos)">
        <xacro:include filename="$(find cambada_description)/urdf/cambada_joint_control.xacro"/>
        <xacro:cambada_joint_control cambada_prefix="${cambada_prefix}"/>
    </xacro:if>

    <xacro:if value="$(arg add_transmission_vel)">
        <xacro:include filename="$(find cambada_description)/urdf/cambada_joint_control_vel.xacro"/>
        <xacro:cambada_joint_control_vel cambada_prefix="${cambada_prefix}"/>
    </xacro:if>


<!--   .................   -->
    <link name="${cambada_prefix}_0_baseA">
		<inertial>
			<origin xyz="-0.0468120874448352 -0.061001599706544 0.151264901631157" rpy="0 0 0" />
			 <mass
        value="0.176066305913454" />
 <inertia
        ixx="8.46236675930541E-05"
        ixy="-2.43581265714865E-19"
        ixz="1.72749951981266E-33"
        iyy="0.00015458992050152"
        iyz="-4.8876551304495E-19"
        izz="8.46236675930541E-05" />
    </inertial>
		<visual>
			<origin xyz="0 0 0" rpy="0 0 0" />
			<geometry>
				<mesh filename="package://cambada_description/meshes/cambada/base.stl" />
			</geometry>
			<material name="">
			<color
          rgba="0.792156862745098 0.819607843137255 0.933333333333333 1" />
      </material>
		</visual>
		<collision>
			<origin xyz="0 0 0" rpy="0 0 0" />
			<geometry>
				<mesh filename="package://cambada_description/meshes/cambada/base.stl" />
			</geometry>
		</collision>
	</link>


	<!-- ...........................				-->

    <link name="${cambada_prefix}_link1">
		<inertial>
			<origin
        xyz="-0.0675103786422473 -0.0106361847738329 0.000302839566712121"
        rpy="0 0 0" />
			<mass value="0.506523908029088"/>
 	<inertia
        ixx="0.00039868453652954"
        ixy="-0.000236947604944033"
        ixz="6.89388961086757E-06"
        iyy="0.00205275796454067"
        iyz="1.06009609216108E-06"
        izz="0.00192159481801324" />
		</inertial>
		<visual>
			<origin xyz="0 0 0" rpy="0 0 0" />
			<geometry>
				<mesh filename="package://cambada_description/meshes/cambada/link1.stl"/>
			</geometry>
			<material name="">
				<color
          rgba="0.792156862745098 0.819607843137255 0.933333333333333 1" />
			</material>
		</visual>
		<xacro:testSphere/>
		<collision>
			<origin xyz="0 0 0" rpy="0 0 0" />
			<geometry>
				<mesh filename="package://cambada_description/meshes/cambada/link1.stl" />
			</geometry>
		</collision>
	</link>
    <joint name="${cambada_prefix}_joint1" type="continuous">
		<origin
      xyz="-0.046812 -0.15126 0"
      rpy="2.4823 1.5708 -1.1118E-10" />
	<xacro:joint_1/>   
   <xacro:default_damping_low/>
	<parent link="${cambada_prefix}_0_baseA" />
        <child link="${cambada_prefix}_link1" />
	
		<axis xyz="1 0 0" />
	</joint>

    <link name="${cambada_prefix}_link2">
		<inertial>
			<origin
        xyz="-0.0052581046729946 0.00295450857829935 -0.0537505494372412"
        rpy="0 0 0" />
			<mass value="0.122671465740391"/>
			<inertia
        ixx="6.26920072741158E-05"
        ixy="6.17013936935818E-06"
        ixz="3.43056355471366E-05"
        iyy="0.000150363943161172"
        iyz="-5.30037376900811E-06"
        izz="0.000128521753047012" />
    </inertial>
		<visual>
			<origin xyz="0 0 0" rpy="0 0 0" />
			<geometry>
				<mesh filename="package://cambada_description/meshes/cambada/link2.stl" />
			</geometry>
			<material name="">
				<color rgba="0.792156862745098 0.819607843137255 0.933333333333333 1" />
			</material>
		</visual>
		<xacro:testSphere/>
		<collision>
			<origin xyz="0 0 0" rpy="0 0 0" />
			<geometry>
				<mesh filename="package://cambada_description/meshes/cambada/link2.stl" />
			</geometry>
		</collision>
	</link>
    <joint name="${cambada_prefix}_joint2" type="revolute">
		<origin
      xyz="-0.1295 -0.01095 -0.012354"
      rpy="-0.0076878 1.5708 9.3263E-07" />
	<xacro:joint_2/>       
   <xacro:default_damping_low/>
        <parent link="${cambada_prefix}_link1" />
        <child link="${cambada_prefix}_link2" />
		<axis xyz="-1 0 0" />
	</joint>


	<!-- ...........................				-->


    <link name="${cambada_prefix}_link3">
		<inertial>
			<origin
        xyz="-0.00010425481448248 1.1663597900391E-05 0.137223691228798"
        rpy="0 0 0" />
			 <mass
        value="0.112859942236474" />
      <inertia
        ixx="0.000431983754640528"
        ixy="-1.36122033756554E-10"
        ixz="-7.7274587076966E-07"
        iyy="0.000427800718144121"
        iyz="3.71653142349756E-08"
        izz="2.01558026238431E-05" />
    </inertial>
		<visual>
			<origin xyz="0 0 0" rpy="0 0 0" />
			<geometry>	
				<mesh filename="package://cambada_description/meshes/cambada/link3.stl" />
			</geometry>
			<material name="">
				<color rgba="0.792156862745098 0.819607843137255 0.933333333333333 1" />
			</material>
		</visual>
		<xacro:testSphere/>
		<collision>
			<origin xyz="0 0 0" rpy="0 0 0" />
			<geometry>
				<mesh filename="package://cambada_description/meshes/cambada/link3.stl" />
			</geometry>
		</collision>
	</link>
    <joint name="${cambada_prefix}_joint3" type="revolute"> 
		<xacro:joint_3/> 
	 	<xacro:default_damping_medium/>
		<origin
      xyz="-0.012353 0 -0.085114"
      rpy="-3.1416 -1.1857E-15 -1.5993" />
	<xacro:joint_3/>       
   <xacro:default_damping_low/>
        <parent link="${cambada_prefix}_link2" />
        <child link="${cambada_prefix}_link3" />
		<axis xyz="0 0 1" />
	</joint>



	<!-- ...........................				-->


    <link name="${cambada_prefix}_link4">
		<inertial>
			 <origin
         xyz="0.00778451999637574 0.00216633639955609 -0.0556062005450023"
        rpy="0 0 0" />
			<mass value="0.077220673843609"/>
			<inertia
        ixx="4.87992086500317E-05"
        ixy="1.13165529904328E-06"
        ixz="8.59372469864067E-06"
        iyy="7.57589120641729E-05"
        iyz="-2.56082370744263E-06"
        izz="5.64292070533432E-05" />
		</inertial>
		<visual>
			<origin xyz="0 0 0" rpy="0 0 0" />
			<geometry>
				<mesh filename="package://cambada_description/meshes/cambada/link4.stl" />
			</geometry>
			<material name="">
				 <color
          rgba="0.792156862745098 0.819607843137255 0.933333333333333 1" />
      </material>
		</visual>
		<xacro:testSphere/>
		<collision>
			<origin xyz="0 0 0" rpy="0 0 0" />
			<geometry>
				<mesh filename="package://cambada_description/meshes/cambada/link4.stl" />
			</geometry>
		</collision>
	</link>
    <joint name="${cambada_prefix}_joint4" type="revolute">
		<origin
      xyz="0 -0.0056551 0.18879"
      rpy="-3.1362 3.5514E-15 -1.5708" />
	<xacro:joint_4/>   
   <xacro:default_damping_low/>    
        <parent link="${cambada_prefix}_link3" />
        <child link="${cambada_prefix}_link4" />
		<axis xyz="-1 0 0" />
	</joint>



	<!-- ...........................				-->


    <link name="${cambada_prefix}_link5">
		<inertial>
			<origin
        xyz="-2.77555756156289E-16 0.000962214925046764 0.130841937326839"
        rpy="0 0 0" />
			<mass value="0.121907728775286"/>
			<inertia
 ixx="0.000439377440553535"
        ixy="2.71050543121376E-20"
        ixz="-1.42301535138722E-18"
        iyy="0.000439317684116605"
        iyz="-4.24697533904001E-06"
        izz="3.06369219328482E-05" />
    </inertial>
		<visual>
			<origin xyz="0 0 0" rpy="0 0 0" />
			<geometry>
				<mesh filename="package://cambada_description/meshes/cambada/link5.stl" />
			</geometry>
			<material name="">
				<color rgba="0.792156862745098 0.819607843137255 0.933333333333333 1" />
			</material>
		</visual>
	 	<xacro:testSphere/>
		<collision>
			<origin xyz="0 0 0" rpy="0 0 0" />
			<geometry>
				<mesh filename="package://cambada_description/meshes/cambada/link5.stl" />
			</geometry>
		</collision>
	</link>
    <joint name="${cambada_prefix}_joint5" type="revolute"><!-- gazebo has a problem with joint limits where lower limit > higher limit (it internally swaps them around). That's why it's limits under -PI. If I chose 2.5..0.73 as limit, gazebo would only let me move the joints within 0.73..2.5. For now, keep it at -3.943..0.81 and publish joint angles NOT between -PI and PI for this joint (otherwise moveit! would recognise e.g. 3. as illegal, although it's legal).-->
	 	<xacro:default_damping_low/>
		<origin
      xyz="-0.0056551 0 -0.085944"
      rpy="-3.1416 -2.4984E-15 -3.0677" />
	<xacro:joint_5/>       
        <parent link="${cambada_prefix}_link4" />
        <child link="${cambada_prefix}_link5" />
		<axis xyz="0 0 1" />
	</joint>

<!-- ......................... -->
    <link name="${cambada_prefix}_link6">


 <inertial>
      <origin
        xyz="0.0240288296393798 -0.00319621828765163 -0.0464369730478372"
        rpy="0 0 0" />
      <mass
        value="0.0219582135312043" />
      <inertia
       ixx="2.79958992740257E-05"
        ixy="-5.82075477860346E-06"
        ixz="9.92987155431784E-07"
        iyy="4.28662514029286E-05"
        iyz="2.35129609173143E-06"
        izz="2.4503666058738E-05" />
    </inertial>
    <visual>
      <origin
        xyz="0 0 0"
        rpy="0 0 0" />
      <geometry>
        <mesh
          filename="package://cambada_description/meshes/cambada/link6.stl" />
      </geometry>
      <material
        name="">
        <color
          rgba="0.792156862745098 0.819607843137255 0.933333333333333 1" />
      </material>
    </visual>
    <collision>
      <origin
        xyz="0 0 0"
        rpy="0 0 0" />
      <geometry>
        <mesh
          filename="package://cambada_description/meshes/cambada/link6.stl" />
      </geometry>
    </collision>
  </link>
<joint
    name="${cambada_prefix}_joint6"
    type="revolute">
    <origin
       xyz="0.024432 0 0.17796"
      rpy="3.139 -5.3668E-15 3.1416" />
	<xacro:joint_6/>       
    <parent link="${cambada_prefix}_link5" />
        <child link="${cambada_prefix}_link6" />
    <axis
      xyz="-1 0 0" />
  </joint>

	<!-- ...........................				-->
 
	<!-- ...........................	-->
	<!--			FINGERS					-->
	<!-- ...........................	-->


	<!-- ...........................				-->


    <link name="${cambada_prefix}_finger1">
		<inertial>
			<origin xyz="0.0324617875497062 0.0128630195852925 -0.000399999999999734" rpy="0 0 0" />
			<mass value="0.00461975628630201"/>
			<inertia
        			ixx="8.60944332945212E-07"
        ixy="2.09039277141263E-07"
        ixz="5.29395592033938E-23"
        iyy="5.28146446809578E-07"
        iyz="-3.50104194263069E-22"
        izz="8.96755606488918E-07" />
		</inertial>
		<xacro:testSphere/>
		<visual>
			<origin xyz="0 0 0" rpy="0 0 0" />
			<geometry>
				<mesh filename="package://cambada_description/meshes/cambada/finger1.stl" />
			</geometry>
			<material name="">
				<color rgba="0.50 0.50 0.50 1" />
			</material>
		</visual>
		<xacro:testSphere/>
		<collision>
			<origin xyz="0 0 0" rpy="0 0 0" />
			<geometry>
				<mesh filename="package://cambada_description/meshes/cambada/finger1.stl" />
			</geometry>
		</collision>
	</link>
    <joint name="${cambada_prefix}_fing1" type="revolute">
	 	<xacro:damping_fingers/>
		<origin  xyz="0.024432 0 -0.096206"
      rpy="-1.5708 -6.8309E-15 0.56738" />
        <parent link="${cambada_prefix}_link6" />
        <child link="${cambada_prefix}_finger1" />
		<axis xyz="0 1 0" />
		<xacro:default_finger_limits/>
	</joint>


	<!-- ...........................				-->

	<!-- ...........................				-->


    <link name="${cambada_prefix}_finger2">
		<inertial>
			<origin xyz="0.0350096605882907 0.00264470992539445 0.000499999999998613"
        rpy="0 0 0" />
			<mass value="0.004619756286302"/>
<inertia
       ixx="8.60944332945211E-07"
        ixy="2.09039277141262E-07"
        ixz="-3.17637355220363E-22"
        iyy="5.28146446809577E-07"
        iyz="-3.09365549094832E-22"
        izz="8.96755606488915E-07" />
		</inertial>
		<xacro:testSphere/>
		<visual>
			<origin xyz="0 0 0" rpy="0 0 0" />
			<geometry>
				<mesh filename="package://cambada_description/meshes/cambada/finger2.stl" />
			</geometry>
			<material name="">
				<color rgba="0.5 0.5 0.5 1" />
			</material>
		</visual>
		<collision>
			<origin xyz="0 0 0" rpy="0 0 0" />
			<geometry>
				<mesh filename="package://cambada_description/meshes/cambada/finger2.stl" />
			</geometry>
		</collision>
	</link>
    <joint name="${cambada_prefix}_fing2" type="revolute">
	 	<xacro:damping_fingers/>
		<origin  xyz="0.024432 0 -0.10621"
      rpy="-1.5708 6.8309E-15 -2.5742" />
        <parent link="${cambada_prefix}_link6" />
        <child link="${cambada_prefix}_finger2" />
		<axis xyz="0 1 0" />
		<xacro:default_finger_limits/>
	</joint>


	<!-- ...........................				-->

	
	</xacro:macro>
</root>
