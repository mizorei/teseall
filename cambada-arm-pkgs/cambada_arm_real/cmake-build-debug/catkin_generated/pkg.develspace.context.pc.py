# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "".split(';') if "" != "" else []
PROJECT_CATKIN_DEPENDS = "moveit_core;moveit_ros_planning_interface;interactive_markers;roscpp;sensor_msgs;std_msgs".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "".split(';') if "" != "" else []
PROJECT_NAME = "cambada_real_arm"
PROJECT_SPACE_DIR = "/home/ema/catkin_ws/src/cambada-arm-pkgs/cambada_arm_real/cmake-build-debug/devel"
PROJECT_VERSION = "0.0.0"
