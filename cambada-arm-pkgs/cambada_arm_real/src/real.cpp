#include <cstdlib>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <ros/subscribe_options.h>
#include "ros/ros.h"
#include "std_msgs/String.h"
#include "sensor_msgs/JointState.h"
#include "std_msgs/Float64.h"
/**************************/
/*MOVEIT*/
#include <moveit/move_group_interface/move_group.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>

#include <moveit_msgs/DisplayRobotState.h>
#include <moveit_msgs/DisplayTrajectory.h>

#include <moveit_msgs/AttachedCollisionObject.h>
#include <moveit_msgs/CollisionObject.h>
void Leitura(const sensor_msgs::JointState::ConstPtr& msg){
    sensor_msgs::JointState test;
    test = *msg;
    int i;

    //one run


    printf("Joint | position\n%10s| %4.2f  \n%10s | %4.2f\n"
                   "%10s | %4.2f  \n%10s | %4.2f\n"
                   "%10s | %4.2f  \n",test.name.at(0).data(), test.position.at(0)*180/3.14,test.name.at(1).data(), test.position.at(1)*180/3.14,
           test.name.at(2).data(), test.position.at(2)*180/3.14, test.name.at(3).data(), test.position.at(3)*180/3.14,
           test.name.at(4).data(), test.position.at(4)*180/3.14);
}

int main(int argc , char** argv) {

    ros::init(argc, argv, "real");

    std_msgs::Float64 cmd;
    ros::NodeHandle nh;
    ros::Subscriber sub = nh.subscribe("/cambada/joint_states", 1, Leitura);
//    ros::Publisher joint1 = nh.advertise<std_msgs::Float64>("/cambada/joint1_position_controller/command", 1);
    ros::AsyncSpinner spinner(1);
    spinner.start();

    moveit::planning_interface::MoveGroup group("Arm"); //Full_controler?
    moveit::planning_interface::PlanningSceneInterface planning_scene_interface;

//parametros de possicao do endfacto
    double a=1;
    geometry_msgs::Pose target_pose1;
    while (a<2) {
        target_pose1.orientation.w = 1.0;
        printf("\n x: ");
        scanf("%lf", &a);
        target_pose1.position.x = a;
        printf("\n y: ");
        scanf("%lf", &a);
        target_pose1.position.y =a;
        printf("\n z: ");
        scanf("%lf", &a);
        target_pose1.position.z = a;
        group.setPoseTarget(target_pose1);
//planeamento do movimento (cinematica inversa)
        moveit::planning_interface::MoveGroup::Plan my_plan;
        bool success = group.plan(my_plan);

        ROS_INFO("Visualizing plan 1 (pose goal) %s", success ? "" : "FAILED");
        if (success) {
            group.move();   //supostamente é para comecar a movimentacao..
            printf("anda");
        }
    }
    return 0;
}
