#ifndef DX_COMM_H_
#define DX_COMM_H_

#include "DevComm.h"

#define DEBUG_PRINTS 0

enum ErrType
{
	ERR_TIMEOUT = 0
};

/**
 * \brief Class to handle the communication with Dynamixel
 * This class is the main interface with the DETI Dynamixel board.
 * It possesses functions to interact with Dynamixel devices in all the ways defined in the available protocol \link dxProtocol.h \endlink.
 */
class DxComm {
public:
	DxComm(DevComm* dc) { transmitter = dc; }

	void dxl_reset(int series, int id);
	bool dxl_ping(int series, int id);
	void dxl_action(int series);
	int dxl_read_byte(int series, int id, int address);
	void dxl_write_byte(int series, int id, int address, int value);
	int dxl_read_word(int series, int id, int address);
	void dxl_write_word(int series, int id, int address, int value);
	void dxl_syncWrite_byte(int series, int nServos, int initialAddress, int *idArray, int *valuesArray);
	void dxl_syncWrite_word(int series, int nServos, int initialAddress, int *idArray, int *valuesArray);
	void dxl_regWrite_byte(int series, int id, int address, int value);
	void dxl_regWrite_word(int series, int id, int address, int value);

	void gotoPosition(int series, int id, int position, int speed);
	void writePosition(int series, int id, int position, int speed);
	int readPosition(int series, int id);

private:
	void printError(ErrType err);
	int getDataFromRxPacket();
	bool timedOut(char errByte);
	bool hasError(char errByte);

private:
	char txPacket[512];
	char rxPacket[256];
	unsigned char rxDataBytes[3];	/*!<Array for the bytes from the response: ErByte : dataByte || dataLow : dataHigh*/

	DevComm* transmitter;
};

#endif /* DX_COMM_H_ */
