#ifndef _DYNAMIXEL_DEFINES
#define _DYNAMIXEL_DEFINES

/******************************************************************************************/
/************************************ MACROS **********************************************/
/******************************************************************************************/

#define MAX_DYNAMIXEL		253			/*!<Definition of the maximum ID allowed for the devices (254 is broadcast address).*/
#define BROADCAST_ID		254

#define AX_SERIES			0			/*!<AX series uses TTL, defines TTL uart on detiDx board*/
#define RX_SERIES			1			/*!<RX series uses RS485, defines RS485 uart on detiDx board*/
#define MX_SERIES			2			/*!<MX series also uses RS485 but has 4096 levels instead of 1024 (only for goal pos). Used only on high level for value validation and translation*/

#define AX12				0x0c
#define AXS1				0x0d
#define IMU6AXIS			0x430a
#define AX18				0x12
#define RX28				0x1c
#define RX64				0x40
#define MX106				0x0140

// Instruction code
#define INST_PING           0x01		// Ping instruction
#define INST_READ           0x02		// Read instruction
#define INST_WRITE          0x03		// Write instruction
#define INST_REG_WRITE      0x04		// Reg_write instruction
#define INST_ACTION         0x05		// Action instruction
#define INST_RESET          0x06		// Reset instruction
#define INST_SYNC_WRITE     0x83		// Sync_write instruction

// Address of control table
//-------------------------- [EEPROM] area -------------------------------------
#define P_MODEL_NUMBER_L      0x00		// Model number lower byte address
#define P_MODEL_NUMBER_H      0x01		// Model number higher byte address
#define P_VERSION             0x02		// Firmware version address
#define P_ID                  0x03		// Dynamixel ID address
#define P_BAUD_RATE           0x04		// Dynamixel baudrate address
#define P_RETURN_DELAY_TIME   0x05		// Return delay time address
#define P_LIMIT_TEMPERATURE   0x0b		// Limited temperature address
#define P_DOWN_LIMIT_VOLTAGE  0x0c		// Down limited voltage address
#define P_UP_LIMIT_VOLTAGE    0x0d		// Up limited voltage address
#define P_RETURN_LEVEL        0x10		// Status return level address

//<<<<<<<<< Motors only >>>>>>>>>>>>
#define P_CW_ANGLE_LIMIT_L    0x06		// CW limited angle lower byte address
#define P_CW_ANGLE_LIMIT_H    0x07		// CW limited angle higher byte address
#define P_CCW_ANGLE_LIMIT_L   0x08		// CCW limited angle lower byte address
#define P_CCW_ANGLE_LIMIT_H   0x09		// CCW limited angle higher byte address
#define P_MAX_TORQUE_L        0x0e		// Max torque lower byte address
#define P_MAX_TORQUE_H        0x0f		// Max torque higher byte address
#define P_ALARM_LED           0x11		// Alarm LED address
#define P_ALARM_SHUTDOWN      0x12		// Alarm shutdown address
//<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>

//<<<<<<<<< MX-106 only >>>>>>>>>>>
#define DRIVE_MODE            0x0a		// MX-106 Dual Mode Setting
//<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>

//<<<<<<<<< AX-S1 only >>>>>>>>>>>>>>>
#define P_EEPROM_IR_DETECT_COMPARE		0x14	// IR obstacle detection compare value address
#define P_EEPROM_LIGHT_DETECT_COMPARE	0x15	// Light detection compare value address
//<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>
//-------------------------------------------------------------------------------

//------------------------------ [RAM] area -------------------------------------
#define P_PRESENT_VOLTAGE			0x2a	// Present voltage address
#define P_PRESENT_TEMPERATURE		0x2b	// Present temperature address
#define P_REGISTERED_INSTRUCTION	0x2c	// Registered instruction address
#define P_LOCK						0x2f	// EEPROM lock flag address

//<<<<<<<<< Motors only >>>>>>>>>>>>
#define P_TORQUE_ENABLE				0x18	// Torque enable flag address
#define P_LED						0x19	// LED on/off flag address
#define P_CW_COMPLIANCE_MARGIN		0x1a	// CW compliance margin address
#define P_CCW_COMPLIANCE_MARGIN		0x1b	// CCW compliance margin address
#define P_CW_COMPLIANCE_SLOPE		0x1c	// CW compliance slope address
#define P_CCW_COMPLIANCE_SLOPE		0x1d	// CCW compliance slope address
#define P_GOAL_POSITION_L			0x1e	// Goal position lower byte address
#define P_GOAL_POSITION_H			0x1f	// Goal position higher byte address
#define P_GOAL_SPEED_L				0x20	// Goal speed lower byte address
#define P_GOAL_SPEED_H				0x21	// Goal speed higher byte address
#define P_TORQUE_LIMIT_L			0x22	// Limited torque lower byte address
#define P_TORQUE_LIMIT_H			0x23	// Limited torque higher byte address
#define P_PRESENT_POSITION_L		0x24	// Present position lower byte address
#define P_PRESENT_POSITION_H		0x25	// Present position higher byte address
#define P_PRESENT_SPEED_L			0x26	// Present speed lower byte address
#define P_PRESENT_SPEED_H			0x27	// Present speed higher byte address
#define P_PRESENT_LOAD_L			0x28	// Present load lower byte address
#define P_PRESENT_LOAD_H			0x29	// Present load higher byte address
#define P_MOVING					0x2e	// Moving state flag address
#define P_PUNCH_L					0x30	// Punch lower byte address
#define P_PUNCH_H					0x31	// Punch higher byte address
//<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>

//<<<<<<<<< MX-106 only >>>>>>>>>>>
#define P_D_GAIN					0x1a	// D component of PID control
#define P_I_GAIN					0x1b	// I component of PID control
#define P_P_GAIN					0x1c	// P component of PID control
#define P_CURRENT_L					0X44	// Lowest byte of Consuming Current
#define P_CURRENT_H					0X45	// Highest byte of Consuming Current
#define P_TORQUE_CONTROL_MODE		0X46	// Torque Control Mode Enable on/off
#define P_GOAL_TORQUE_L				0X47	// Lowest byte of goal torque value
#define P_GOAL_TORQUE_H				0X48	// Highest byte of goal torque value
#define P_GOAL_ACCELERATION			0X49	// Goal Acceleration
//<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>

//<<<<<<<<< AX-S1 only >>>>>>>>>>>>>>>
#define P_IR_LEFT_FIRE_DATA			0x1a	// Left IR detection value address
#define P_IR_CENTER_FIRE_DATA		0x1b	// Center IR detection value address
#define P_IR_RIGHT_FIRE_DATA		0x1c	// Right IR detection value address
#define P_LIGHT_LEFT_DATA			0x1d	// Left light detection value address
#define P_LIGHT_CENTER_DATA			0x1e	// Center light detection value address
#define P_LIGHT_RIGHT_DATA			0x1f	// Right light detection value address

#define P_IR_OBSTACLE_DETECTED		0x20	// Left IR detection value address
#define P_LIGHT_DETECTED			0x21	// Center IR detection value address
#define P_SOUND_DATA				0x23	// Sound data address
#define P_SOUND_DATA_MAX_HOLD		0x24	// Sound data max hold value address
#define P_SOUND_DETECTED_COUNT		0x25	// Sound data detected count value address
#define P_SOUND_DETECTED_TIME_L		0x26	// Sound detected time value lower byte address
#define P_SOUND_DETECTED_TIME_H		0x27	// Sound detected time value higher byte address
#define P_BUZZER_DATA0				0x28	// Buzzer data0 address
#define P_BUZZER_DATA1				0x29	// Buzzer data1 address
#define P_REMOCON_ARRIVED			0x30	// IR remocon arrived flag address
#define P_REMOCON_RXDATA0			0x31	// IR remocon rx data lower byte address
#define P_REMOCON_RXDATA1			0x32	// IR remocon rx data higher byte address
#define P_REMOCON_TXDATA0			0x33	// IR remocon tx data lower byte address
#define P_REMOCON_TXDATA1			0x34	// IR remocon tx data higher byte address
#define P_IR_DETECT_COMPARE			0x35	// IR obstacle detection compare value address
#define P_LIGHT_DETECT_COMPARE		0x36	// Light detection compare value address
//<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>

//<<<<<<<<< 6-AXIS-IMU only >>>>>>>>>>>>>>>
#define P_LED						0x19	// LED on/off flag address
#define P_SIDEWAYS_ACCEL_L			0x1a	// Sideways acceleration lower byte address (XX)
#define P_SIDEWAYS_ACCEL_H			0x1b	// Sideways acceleration higher byte address (XX)
#define P_FORWARDS_ACCEL_L			0x1c	// Forward acceleration lower byte address (YY)
#define P_FORWARDS_ACCEL_H			0x1d	// Forward acceleration higher byte address (YY)
#define P_VERTICAL_ACCEL_L			0x1e	// Vertical acceleration lower byte address (ZZ)
#define P_VERTICAL_ACCEL_H			0x1f	// Vertical acceleration higher byte address (ZZ)
#define P_PITCH_RATE_L				0x20	// Pitch angle lower byte address (YY)
#define P_PITCH_RATE_H				0x21	// Pitch angle higher byte address (YY)
#define P_ROLL_RATE_L				0x22	// Roll angle lower byte address (XX)
#define P_ROLL_RATE_H				0x23	// Roll angle higher byte address (XX)
#define P_YAW_RATE_L				0x24	// Yaw angle lower byte address (ZZ)
#define P_YAW_RATE_H				0x25	// Yaw angle higher byte address (ZZ)

//-------------------------------------------------------------------------------

// Error code bit of status packet
// #define ERROR_NONE				0x00	// All bit cleared
// #define ERROR_VOLTAGE			0x01	// 0th error bit	'00000001'	- Voltage error
// #define ERROR_ANGLE				0x02	// 1th error bit	'00000010'	- Limited angle error (Motor only)
// #define ERROR_OVERHEAT			0x04	// 2th error bit	'00000100'	- Overheatting error
// #define ERROR_RANGE				0x08	// 3th error bit	'00001000'	- Range error
// #define ERROR_CHECKSUM			0x10	// 4th error bit	'00010000'	- Packet's checksum error
// #define ERROR_OVERLOAD			0x20	// 5th error bit	'00100000'	- Overload error (Motor only)
// #define ERROR_INSTRUCTION		0x40	// 6th error bit	'01000000'	- Instruction code error

// Alarm LED bit of P_ALARM_LED (Motor only)
#define ALARM_NONE				0x00	// All bit cleared
#define ALARM_VOLTAGE			0x01	// 0th alram bit	'00000001'	- Voltage error alarm
#define ALARM_ANGLE				0x02	// 1th alram bit	'00000010'	- Limited angle error alarm
#define ALARM_OVERHEAT			0x04	// 2th alram bit	'00000100'	- Overheating error alarm
#define ALARM_RANGE				0x08	// 3th alram bit	'00001000'	- Range error alarm
#define ALARM_CHECKSUM			0x10	// 4th alram bit	'00010000'	- Packet's checksum error alarm
#define ALARM_OVERLOAD			0x20	// 5th alram bit	'00100000'	- Overload error alarm
#define ALARM_INSTRUCTION		0x40	// 6th alram bit	'01000000'	- Instruction code error alarm

// Alarm Shutdown bit of P_ALARM_SHUTDOWN (Motor only)
#define SHUTDOWN_NONE			0x00	// All bit cleared
#define SHUTDOWN_VOLTAGE		0x01	// 0th shutdown bit	'00000001'	- Voltage error shutdown
#define SHUTDOWN_ANGLE			0x02	// 1th shutdown bit	'00000010'	- Limited angle error shutdown
#define SHUTDOWN_OVERHEAT		0x04	// 2th shutdown bit	'00000100'	- Overheatting error shutdown
#define SHUTDOWN_RANGE			0x08	// 3th shutdown bit	'00001000'	- Range error shutdown
#define SHUTDOWN_CHECKSUM		0x10	// 4th shutdown bit	'00010000'	- Packet's checksum error shutdown
#define SHUTDOWN_OVERLOAD		0x20	// 5th shutdown bit	'00100000'	- Overload error shutdown
#define SHUTDOWN_INSTRUCTION	0x40	// 6th shutdown bit	'01000000'	- Instruction code error shutdown

// Value of P_ID
#define BROADCASTING_ID		0xfe	// Broadcasting ID

// Value of P_RETURN_LEVEL
#define RETURN_NONE		0x00		// Return no status packet
#define RETURN_READ		0x01		// Return read instruction only
#define RETURN_ALL		0x02		// Return all instruction

// Macro of direction
#define CCW_DIRECTION	0x00		// CCW direction value
#define CW_DIRECTION	0x01		// CW direction value

// LED state of P_LED (Motor only)
#define LED_OFF			0x00		// Turn off LED
#define LED_ON			0x01		// Turn on LED

// Torque state P_TORQUE_ENABLE (Motor only)
#define TORQUE_OFF		0x00		// Turn off Torque
#define TORQUE_ON		0x01		// Turn on Torque

// Moving state of P_MOVING (Motor only)
#define NOT_MOVING		0x00		// Stop state
#define MOVING			0x01		// Moving state

// Miscellaneous
#define MAKE_WORD(a, b)		((WORD) (((BYTE) (a)) | ((WORD) ((BYTE) (b))) << 8))
#define HIGH_BYTE(w)		((BYTE) (((WORD) (w) >> 8) & 0xFF))
#define LOW_BYTE(w)			((BYTE) (w))
#define MAKE_VALUE(d, v)	((WORD) (((WORD) (v)) | (((WORD) (d)) << 10)))
#define DIRECTION(w)		((BYTE) ((((WORD) (w)) & 0x400) >> 10))
#define VALUE(w)			((WORD) (((WORD) (w)) & 0x3ff))


/******************************************************************************************/
/******************************************************************************************/

#endif
