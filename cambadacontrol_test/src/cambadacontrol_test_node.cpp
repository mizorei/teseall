//http://clopema.felk.cvut.cz/redmine/projects/clopema/wiki/Sending_trajectory_to_the_controller
//http://wiki.ros.org/actionlib_tutorials/Tutorials/SimpleActionServer%28ExecuteCallbackMethod%29
//http://wiki.ros.org/actionlib_tutorials/Tutorials/SimpleActionServer%28GoalCallbackMethod%29
#include <ros/ros.h>
#include <actionlib/server/simple_action_server.h>
#include <control_msgs/FollowJointTrajectoryAction.h>
#include <trajectory_msgs/JointTrajectory.h>
#include <sensor_msgs/JointState.h>

#include "detiDxDriver/DxComm.h"
#include "detiDxDriver/DevCommUsb.h"
#include "dynamixelDefines.h"


DxComm communicator = DxComm(new DevCommUsb());
int ID[5]= {6,4,1,2,3};
double currentPos[9]={0,0,0,0,0,0,0,0,0};
double targetPos[7];
double targetVel[7];

/*class RobotJointConTroller {
  protected:
  ros::NodeHandle nh_;
  sensor_msgs::JointState as_;
  std::string action_name_;
  ros::Subscriber sub;

  public:
  RobotJointConTroller(std::string name):
  as_(nh_, name,false),
  action_name_(name){

  }

  };*/


class RobotTrajectoryFollower {
	protected:

		ros::NodeHandle nh_;
		// NodeHandle instance must be created before this line. Otherwise strange error may occur.
		actionlib::SimpleActionServer <control_msgs::FollowJointTrajectoryAction> as_;
		std::string action_name_;

		double goal_[9];


		//actionlib_tutorials::AveragingFeedback feedback_;
		// actionlib_tutorials::AveragingResult result_;
		ros::Subscriber sub_;
	public:

		RobotTrajectoryFollower(std::string name) :
			as_(nh_, name, false),
			action_name_(name) {
				//Register callback functions:
				as_.registerGoalCallback(boost::bind(&RobotTrajectoryFollower::goalCB, this));
				as_.registerPreemptCallback(boost::bind(&RobotTrajectoryFollower::preemptCB, this));
				as_.start();

			}

		~RobotTrajectoryFollower(void)//Destructor
		{
		}

		void goalCB() {
			// accept the new goal
			trajectory_msgs::JointTrajectory_<std::allocator<void>>::_points_type points;

			unsigned long size;
			trajectory_msgs::JointTrajectory traj;
			// points = as_.acceptNewGoal()->trajectory.points;
			traj =  as_.acceptNewGoal()->trajectory;
			int j;
			printf("joint name [");
			for (j=0;j<8;j++) {
				printf(" %s,", traj.joint_names.at(j).data());
			}
			printf("] \n");
			points = traj.points;
			size = points.size();

			unsigned long i;
			printf("positions: [");
			for (i = 0; i < 8; i++){
				goal_[i] = points[size-1].positions.at(i);
				printf("%f, ",goal_[i]);
			}

            //////
            //codigo de teste para a velocidade
            int k;
            double velocity[8]= {0,0,0,0,0,0,0,0};
            for (j=0; j<size-1;j++){
                for(k=0;k<8;k++){
                    if(velocity[k]< fabs(points[j].velocities.at(k)))
                        velocity[k] = fabs(points[j].velocities.at(k));
                }
            }
         /*   for (i = 0; i < 8; i++){
                printf("\nmax velocity -> %f",velocity[i]) ;
            }*/

            //////


			printf("]\n");
			//--------PARA TESTES DEPOIS FAZER O ROBOT STATE
			for (i = 0; i <5 ; ++i) {
				targetPos[i]=goal_[i+3];
                targetVel[i]=velocity[i+3];
			}
			currentPos[1]=goal_[2];
            //currentVel[1]=vel_[2];
			write();
		}

		void write(void){
			double con=180/3.14;
            double convel=30/(3.14*0.114); //o valor 0.111 serve para o calculo da velocidade da serie RX
                                            //paga o MX deveria de ser 0.114 esta info esta disponivel no datasheet dos
                                            //dynamixel
			int i;
			for(i=0;i<5;i++) {
                if(i>2) {
                    convel = 30 / (3.14 * 0.111);   //actualiza para os servos RX
                }
				joint(ID[i], (int) (targetPos[i] * con), (int)(targetVel[i]*convel) );
				usleep(200);
			}
			go();
			usleep(2);
			go();
		}
		void preemptCB() {
			ROS_INFO("%s: Preempted", action_name_.c_str());
			// set the action state to preempted
			as_.setPreempted();
		}


		void joint(int id,int pos,int speed){
			int serie;

			double a=3.4111111,c=307;
			double a2= 11.377777, c2=1024,c22=2048;
			if (pos > 90)
				pos=85;
			else if(pos<-90)
				pos=-85;
			if(id==6){
                pos = (int) (a2*pos+c22);
                serie = MX_SERIES;
                if( speed <1)
                    return;
                else if(speed <10)
                speed=10;
            }else if(id==4 || id==1){
				pos = (int) (a2*pos+c2);
				serie = MX_SERIES;
				if( speed <1)
					return;
                else if(speed <10)
                    speed=10;
			}else{
				pos= (int) (a*pos+c);
				serie = RX_SERIES;
				if (speed < 1)
					return;
                else if(speed <10)
                    speed=10;
			}
			if(speed>80)
				speed=80;

			if(id==3)
				printf("\n");
          //  printf("\ncomunicatio with ID= %d | with pos -> %d | with vel -> %d",id,pos,speed);
			communicator.writePosition(serie,id,pos,speed);
		}

		void go(){
			int serie[2] = {MX_SERIES,RX_SERIES};
			communicator.dxl_action(serie[0]);
			usleep(200);
			communicator.dxl_action(serie[1]);
		}


};


void read(void){
	double a1 = 0.29325, c1 = -90, c2=-180;
	double a2 = 0.08789;
	double conv=3.14/180; //variavel de conversao degrees to rad
	int i,serie=MX_SERIES;
	for (i = 2; i < 7; i++) {
		if (i > 4)
			serie = RX_SERIES;
		currentPos[i]=communicator.readPosition(serie, ID[i-2]);
	}
    currentPos[2] = (a2 * currentPos[2] + c2)*conv;
	for(i=3;i<5;i++){
		currentPos[i] = (a2 * currentPos[i] + c1)*conv;
	}
	for (i = 5; i < 7; i++) {
		currentPos[i] = (a1 * currentPos[i] + c1)*conv;
	}
}

//--------------------------------- Make Class for this----------------
double GP[9]; //goal position of the joint controller


void joint(int id,int pos,int speed){
	int serie;
	printf("pos: %d",pos);
	double a=3.4111111,c=307;
	double a2= 11.377777, c2=1024,c22=2048;
	if (pos > 90)
		pos=90;
	else if(pos<-90)
		pos=-90;
	if(id==6){
        pos = (int) (a2*pos+c22);
        serie = MX_SERIES;
        if( speed <1)
            speed = 3;
    }
    else if(id==4 || id==1){
		pos = (int) (a2*pos+c2);
		serie = MX_SERIES;
		if( speed <1)
			speed = 3;
	}else{
		pos= (int) (a*pos+c);
		serie = RX_SERIES;
		if (speed < 1)
			speed =3;
	}
	if(speed>100)
		speed=100;
    else if(speed < 2)
        speed =2;
	printf("\ncomunicatio with ID= %d | with pos -> %d",id,pos);
	if(id==3)
		printf("\n");
	communicator.writePosition(serie,id,pos,speed);
}

void go(){
	int serie[2] = {MX_SERIES,RX_SERIES};
	communicator.dxl_action(serie[0]);
	usleep(200);
	communicator.dxl_action(serie[1]);
}


void write(void){
	double con=180/3.14;
	int i;
	for(i=0;i<5;i++) {
		joint(ID[i], (int) (GP[i] * con), 50); //criar funcao para regular a velocidade dependendo da maxima distancia percorrida.
		usleep(200);
	}
	go();
	usleep(2);
	go();
}



void Jocontrol(const sensor_msgs::JointState::ConstPtr& msg) {
	sensor_msgs::JointState Goal;
	Goal.position = msg->position;
	int j;
	for(j=0;j<5;j++ ) {
		GP[j] = Goal.position[j+1];
	}
	write();
}

//---------------------------End of call to be ---------------------
int main(int argc, char **argv) {
	ros::init(argc, argv, "hardware_interface");

	ros::NodeHandle nh;
	ros::Publisher robotState = nh.advertise<sensor_msgs::JointState>("/cambada/joint_state",1);
	RobotTrajectoryFollower RobotTrajectoryFollower("/cambada/joint_trajectory_action");

	ros::Subscriber sub = nh.subscribe("/cambada/joint_control",1,Jocontrol);
	sensor_msgs::JointState robotData;
	robotData.name.push_back("arm_on_table");       //0
	robotData.name.push_back("cambada_joint1");     //shoulder rotate
	robotData.name.push_back("cambada_joint2");     //shoulder pitch
	robotData.name.push_back("cambada_joint3");     //rotational
	robotData.name.push_back("cambada_joint4");     //elbow
	robotData.name.push_back("cambada_joint5");     //wrist rotation
	robotData.name.push_back("cambada_joint6");     //wrist pitch
	robotData.name.push_back("cambada_fing1");
	robotData.name.push_back("cambada_fing2");
	int i=0;
	for (i=0;i<9;i++) {
		robotData.position.push_back(0);
	}

	ros::Rate loop_rate(3);
	while(ros::ok()) {
		//ler o estado do robo no momento
		read();
		for(i=1;i<8;i++){
			robotData.position[i] =currentPos[i];
		}
		//publicar os estados do robo
		robotData.header.stamp = ros::Time::now();
		robotState.publish(robotData);
		//escrever dados para o robo
		//write();

		ros::spinOnce();
		loop_rate.sleep();
	}
	return 0;
}
