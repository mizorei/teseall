#ifndef DEV_COMM_H_
#define DEV_COMM_H_

/**
 * \brief Abstract class to handle the communication with the device
 * This class has the base methods to open, close and send/receive a packet over the used device.
 * The class is fully virtual, a child from this class must implement the functions according to the target OS and device management.
 */
class DevComm {
public:
	DevComm(){};
	virtual ~DevComm(){};

	/*!This function will be responsible for opening the communication channel using the device available.*/
	virtual void openChannel() = 0;

	/*!This function will be responsible for closing the communication channel.*/
	virtual void closeChannel() = 0;

	/*!This function will be responsible for dealing with the "physical" transmission of the data.
	 * It will then proceed to listening for the correspondent response. Whoever calls this function is responsible for guaranteeing that the reception buffer is large enough for rxSize+1 (+1 for '\0')
	 * \param txPacket The data to be transmitted, provided by the \link DxComm \endlink method that invokes the transmission.
	 * \param rxPacket The array to store the response from the deti DX board. The \link DxComm \endlink method invoking the transmission will further handle its data.
	 * \param rxSize The maximum size of data on the response packet corresponding to the invoking method.
	 * \return a boolean value indicating if the transmission/reception was performed. True in the normal situation, False in case of timeout on reception or channel error.*/
	virtual bool txRx(const char* txPacket, char* rxPacket, int rxSize) = 0;
};

#endif /* DEV_COMM_H_ */
