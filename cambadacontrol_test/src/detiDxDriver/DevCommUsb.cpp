#include "DevCommUsb.h"
#include <signal.h>
#include <sys/time.h>

#define TIMEOUT_MICROSECONDS 100000

enum { PARITY_NONE, PARITY_ODD, PARITY_EVEN, PARITY_MARK, PARITY_SPACE };
bool alarmOn = false;

// Static function launched by the alarm signal. Sets the interrupt boolean flag to true.
static void alarmCk (int signum)
{
	if (signum == SIGALRM)
		alarmOn = true;
}

DevCommUsb::DevCommUsb(){
	this->devname = "/dev/ttyUSB0";
	resetAttributes();
	openChannel();
}

DevCommUsb::DevCommUsb(const char* devName){
	this->devname = devName;
	resetAttributes();
	openChannel();
}

DevCommUsb::~DevCommUsb(){
	if (fd != -1)
	{
		closeChannel();
	}
}

void DevCommUsb::resetAttributes(){
	baudrate = 115200;
	databits = 8;
	stopbits = 1;
	parity = PARITY_NONE;
	fd = -1;
}

void DevCommUsb::openChannel(){
	/* open device */
	if ((fd = open(devname, O_RDWR | /* O_NOCTTY | */ O_NDELAY)) < 0)
	{
		fprintf(stderr, "CommChannel::openChannel(\"%s\"): ", devname);
		perror("");
		exit(EXIT_FAILURE);
	}

	// flushing is to be done after opening. This prevents first read and write to be spam'ish. (ACP: from cutecom)
	tcflush(fd, TCIOFLUSH);

	/* ACP: estava em cutecom */
	/** \todo Clarify the purpose of the two following lines */
	int n = fcntl(fd, F_GETFL, 0);
	fcntl(fd, F_SETFL, n & ~O_NDELAY);

	/* save old settings */
	if ((tcgetattr(fd,&oldtio)) == -1)
	{
		perror("CommChannel::CommChannel(): tcgetattr()");
		exit(EXIT_FAILURE);
	}

	/* apply new settings */
	if (setNewSettings(baudrate, databits, PARITY_NONE, stopbits, false, false) == -1)
	{
		exit(EXIT_FAILURE);
	}
}

int DevCommUsb::setNewSettings(int baudrate, int databits, const int parity, const int stopbits, bool softwareHandshake, bool hardwareHandshake) {
    /* get previous settings */
    struct termios newtio;
    if (tcgetattr(fd, &newtio)!=0)
    {
        perror("CommChannel::setNewSettings(): tcgetattr()");
        memset(&newtio, 0, sizeof(newtio));
    }

    /* change input and output baudrates */
    speed_t _baud=0;
    switch (baudrate)
    {
        case 600:    _baud=B600;    break;
        case 1200:   _baud=B1200;   break;
        case 2400:   _baud=B2400;   break;
        case 4800:   _baud=B4800;   break;
        case 9600:   _baud=B9600;   break;
        case 19200:  _baud=B19200;  break;
        case 38400:  _baud=B38400;  break;
        case 57600:  _baud=B57600;  break;
        case 115200: _baud=B115200; break;
        case 230400: _baud=B230400; break;
        case 460800: _baud=B460800; break;
        case 576000: _baud=B576000; break;
        case 921600: _baud=B921600; break;
        // case 128000: _baud=B128000; break;
        // case 256000: _baud=B256000; break;
        default: _baud=B115200; break;
    }
    cfsetospeed(&newtio, (speed_t)_baud);
    cfsetispeed(&newtio, (speed_t)_baud);

    /* We generate mark and space parity ourself. */
    if (databits == 7 && (parity==PARITY_MARK || parity == PARITY_SPACE))
        databits = 8;

    switch (databits)
    {
    case 5:
        newtio.c_cflag = (newtio.c_cflag & ~CSIZE) | CS5;
        break;
    case 6:
        newtio.c_cflag = (newtio.c_cflag & ~CSIZE) | CS6;
        break;
    case 7:
        newtio.c_cflag = (newtio.c_cflag & ~CSIZE) | CS7;
        break;
    case 8:
    default:
        newtio.c_cflag = (newtio.c_cflag & ~CSIZE) | CS8;
        break;
    }
    newtio.c_cflag |= CLOCAL | CREAD;

    // parity
    newtio.c_cflag &= ~(PARENB | PARODD);
    if (parity == PARITY_EVEN)
        newtio.c_cflag |= PARENB;
    else if (parity== PARITY_ODD)
        newtio.c_cflag |= (PARENB | PARODD);

    //hardware handshake
/*
    if (hardwareHandshake)
        newtio.c_cflag |= CRTSCTS;
    else
        newtio.c_cflag &= ~CRTSCTS;
*/
    newtio.c_cflag &= ~CRTSCTS;

    // stopbits
    if (stopbits==2)
        newtio.c_cflag |= CSTOPB;
    else
        newtio.c_cflag &= ~CSTOPB;

    /* set input flags */
//   newtio.c_iflag=IGNPAR | IGNBRK;
    newtio.c_iflag=IGNBRK;
//   newtio.c_iflag=IGNPAR;

    //software handshake
    if (softwareHandshake)
        newtio.c_iflag |= IXON | IXOFF;
    else
        newtio.c_iflag &= ~(IXON|IXOFF|IXANY);

    /* set local flags */
    newtio.c_lflag=0;

    /* set output flags */
    newtio.c_oflag=0;

    /* ??? */
   newtio.c_cc[VTIME]=1;
   // newtio.c_cc[VMIN]=60; // ACP: no original
   newtio.c_cc[VMIN]=1;

    /* ACP: aplica os novos settings */
//   tcflush(fd, TCIFLUSH);
    if (tcsetattr(fd, TCSANOW, &newtio)!=0)
    {
        perror("CommChannel::setNewSettings(): tcsetattr()");
        /* ACP: no original não aborta. Fará sentido? */
    }

    /* ACP: ??? */
    int mcs=0;
//   ioctl(fd, TIOCMODG, &mcs);
    ioctl(fd, TIOCMGET, &mcs);
    mcs |= TIOCM_RTS;
    ioctl(fd, TIOCMSET, &mcs);

    /* ACP: suponho que o ioctl anterior possa alterar os settings (??) */
    if (tcgetattr(fd, &newtio)!=0)
    {
        perror("CommChannel::setNewSettings(): tcgetattr()");
        /* ACP: no original não aborta. Fará sentido? */
    }

    //  hardware handshake
    if (hardwareHandshake)
        newtio.c_cflag |= CRTSCTS;
    else
        newtio.c_cflag &= ~CRTSCTS;

    /* ACP: volta a aplicar os settings (??) */
    if (tcsetattr(fd, TCSANOW, &newtio)!=0)
    {
        perror("CommChannel::setNewSettings(): tcsetattr()");
        /* ACP: no original não aborta. Fará sentido? */
    }

    return 0;
}

void DevCommUsb::closeChannel(){
	/* Repor configuracao inicial da porta serie */
	if ((tcsetattr(fd, TCSANOW, &oldtio)) == -1)
	{
		perror("DevCommUsb::~DevCommUsb(): tcsetattr()");
		return;
	}

	/* Fechar o fd associado a porta serie */
	if (close(fd) == -1)
	{
		perror("DevCommUsb::~DevCommUsb(): close()");
		return;
	}
    fd = -1;
}

bool DevCommUsb::txRx(const char* txPacket, char* rxPacket, int rxSize){
	///////////////////// Send packet code //////////////////////
	int nToSend = strlen(txPacket);
#if DEBUG_PRINTS
	printf("Writing frame \"%s\"\n", txPacket);
#endif
	if ( int a = (write(fd, txPacket, nToSend)) != nToSend)
	{
		fprintf(stderr, "DevCommUsb: failed writing to comm channel should send %d, sent %d\n", nToSend, a);
		return false;
	}

	///////////////////// Read response code ////////////////////

	//ACTIVATE TIMER (must be enough to receive everything)
	/* set the timer */
	struct sigaction act, oact;                                         // action to be introduced and existing action
	struct itimerval titv, otitv;                         // time interval to be introduced and existing time interval

	act.sa_handler = alarmCk;                                              // specification of signal service function
	sigemptyset (&act.sa_mask);                // no other signal will be blocked during the processing of this signal
	act.sa_flags = 0;                                                                              // default behavior
	if (sigaction (SIGALRM, &act, &oact) != 0) {                                               // function registering
		perror ("error on registering the signal function (DR)");
		return EXIT_FAILURE;
	}
	titv.it_interval.tv_sec = 0;
	titv.it_interval.tv_usec = 0;
	titv.it_value.tv_sec = 0;
	titv.it_value.tv_usec = TIMEOUT_MICROSECONDS;
	if ( int ret = (setitimer (ITIMER_REAL, &titv, &otitv)) != 0) {
		fprintf(stderr,"setitimer returned %d\n",ret);
		perror ("error on registering the time interval for interruption (DR)");
		return EXIT_FAILURE;
	}
	// Reset the alarm flag before sending
	alarmOn = false;

	/* wait for start of frame character */
#define DUMMY_RECEIVE 0

#if DUMMY_RECEIVE
	//STX 04 SS 01 ER ED CS
	rxPacket[0] = txPacket[0];
	rxPacket[1] = txPacket[1];
	rxPacket[2] = txPacket[2];
	rxPacket[3] = txPacket[3];
	rxPacket[4] = txPacket[4];

	sprintf(rxPacket+5,"%02X",2);
//	rxPacket[5] = 0x00;
//	rxPacket[6] = 0x01;	//Size of response is 1 (only error byte)

	sprintf(rxPacket+7,"%02X",128+8);
//	rxPacket[7] = 0x00;
//	rxPacket[8] = 0x08;	//Return most significant byte of error code with 1, meaning device didn't respond

	int value = 234;
//	unsigned char valueLow = (unsigned char)value;
//	unsigned char valueHigh = (unsigned char)(value >> 8);
//	sprintf(rxPacket+9,"%02X%02X",valueLow,valueHigh);

	sprintf(rxPacket+9,"%02X",value);

	sprintf(rxPacket+11,"&");
//	rxPacket[9] = '&';

//	unsigned char checksum = 1+0+3+0+valueLow+valueHigh;
	unsigned char checksum = 1+0+2+128+8+value;
	checksum = (~checksum + 1) & 0xff;

	sprintf(rxPacket+12,"%02X",checksum);
//	rxPacket[10] = checksum;
//	rxPacket[11] = checksum >> 8;

	return true;



	printf(" RECEIVED PACKET: ");
	int n;
	char chr;
	while ( (n = read(fd, &chr, 1)) == 1 ) {
		printf("%c",chr);
	}
	printf("\n");

	if (n == -1)
	{
		if (alarmOn)
			return false;

		perror("Comm channel");
		exit(EXIT_FAILURE);
	}
#else
	char chr;
	int n;
	while ((n = read(fd, &chr, 1)) == 1 && chr != '%');

	if (n == 0)
	{
		fprintf(stderr, "DevCommUsb: comm channel seems to be closed, try again?\n");
		exit(EXIT_FAILURE);
	}
	else if (n == -1)
	{
#if DEBUG_PRINTS
		fprintf(stderr,"DevCommUsb: n was -1 BEFORE start of frame\n");
#endif
		if (alarmOn)
			return false;

		perror("Comm channel");
		exit(EXIT_FAILURE);
	}

	/* rxSize is TOTAL number of bytes composing the frame
	 * start of frame ('%') and end data ('&') are actually only one byte, all remainder bytes come in ascii and thus 2 bytes*/
	int withoutCS = rxSize - 2; //withoutCS does NOT include the checksum that comes AFTER '&' (it will be read outside of cycle)

	/* read remainder of the frame */
	char* p = rxPacket;
	int cnt;
//	do
//	{
	/* collect bytes until end of frame character is read */
	p = rxPacket;
	*p++ = chr;
	cnt = 1;

	while (cnt < withoutCS && (n = read(fd, &chr, 1)) == 1 && chr != '&' && chr != '%')
	{
#if DEBUG_PRINTS
		fprintf(stderr,"DevCommUsb: received char: %c\n", chr);
#endif
		*p++ = chr;
		cnt++;
	}

	if (cnt > rxSize)
	{
		fprintf(stderr, "DevCommUsb: frame size exceeds maximum allowed for the asked instruction => INVALID PACKET READ\n");
		return false;
	}
	else if (n == 0)
	{
		fprintf(stderr, "DevCommUsb: comm channel seems to be closed => INVALID PACKET READ\n");
		return false;
	}
	else if (n == -1)
	{
#if DEBUG_PRINTS
		fprintf(stderr,"DevCommUsb: n was -1 AFTER start of frame\n");
#endif
		if (alarmOn)
			return false;

		perror("Comm channel");
		exit(EXIT_FAILURE);
	}
	else if (chr == '%')
	{
		fprintf(stderr, "DevCommUsb: invalid start of frame read => INVALID PACKET READ\n");
		return false;
	}
//	} while (chr != '&');

	/* complete the received frame */
	// Put the '&' character in the frame
	*p++ = chr;
	cnt++;
	// Read the last 2 bytes, with the value of the checksum
	if ((n = read(fd, &chr, 1)) == 1) {
		*p++ = chr;
		cnt++;
	}
	if (n == -1)
	{
		if (alarmOn)
			return false;

		perror("Comm channel");
		exit(EXIT_FAILURE);
	}

	if ((n = read(fd, &chr, 1)) == 1) {
		*p++ = chr;
		cnt++;
	}
	if (n == -1)
	{
		if (alarmOn)
			return false;

		perror("Comm channel");
		exit(EXIT_FAILURE);
	}
	*p = '\0';

	/* frame should have an even number of bytes and at least one less byte than "required" */
	if (cnt % 2 != 0 || cnt < (rxSize-2))
	{
		fprintf(stderr, "DevCommUsb: frame: %s\n", rxPacket);
		fprintf(stderr, "DevCommUsb: invalid frame size %d\n", cnt);
		//exit(EXIT_FAILURE);
	}

#if DEBUG_PRINTS
	printf("Received frame \"%s\"\n", rxPacket);
#endif

	/*! \todo transformar em byte array para verificar checksum*/
#endif

	return true;
}
