#ifndef DEV_COMM_USB_H_
#define DEV_COMM_USB_H_

#include "DevComm.h"
#include <stdio.h>
#include <termios.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <stdlib.h>

#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>

#define DEBUG_PRINTS 0

/**
 * \brief This class implements the device communication for a USB device in Linux (see \link DevComm \endlink for general details)
 */
class DevCommUsb : public DevComm {
public:
	DevCommUsb();
	DevCommUsb(const char* devName);
	~DevCommUsb();

	void openChannel();
	void closeChannel();
	bool txRx(const char* txPacket, char* rxPacket, int rxSize);

private:
	int fd;                         /**< channel file descriptor */
	struct termios newtio;          /**< the new channel settings */
	struct termios oldtio;          /**< the previous channel settings */
	const char* devname;            /**< the device name */
	int baudrate;                   /**< the baudrate */
	int databits;                   /**< number of databits */
	int stopbits;                   /**< number of stopbits */
	int parity;                     /**< the parity */

	int setNewSettings(int baudrate, int databits, const int parity, const int stopbits, bool softwareHandshake, bool hardwareHandshake);
	void resetAttributes();
};

#endif /* DEV_COMM_USB_H_ */
