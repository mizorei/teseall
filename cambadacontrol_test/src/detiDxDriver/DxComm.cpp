#include "DxComm.h"
#include "dxProtocol.h"
#include <stdio.h>

#define ascii2bin(a, b)	(b) = (a) - '0';\
						if( (b) > 9 )\
							(b) = (b) - 7
#define SOFR	'%'
#define EODT	'&'
#define EOFR	'#'

void DxComm::dxl_reset(int series, int id){
	//Command: STX 00 SS 01 ID ED CS
	int dxMsgSize = 1;
	unsigned char dataLength = (DX_RESET + series + dxMsgSize + id);
	unsigned char checksum = (~dataLength + 1) & 0xff;

	sprintf(txPacket,"%%" "%02X%02X%02X%02X&%02X", DX_RESET, series, dxMsgSize, id, checksum);

	//Answer: STX 00 SS 01 ER ED CS
	if (!transmitter->txRx(txPacket, rxPacket, 5*2+2))
		printError(ERR_TIMEOUT);
}

bool DxComm::dxl_ping(int series, int id){
	//Command: STX 01 SS 01 ID ED CS
	int dxMsgSize = 1;
	unsigned char dataLength = (DX_PING + series + dxMsgSize + id);
	unsigned char checksum = (~dataLength + 1) & 0xff;

	sprintf(txPacket,"%%" "%02X%02X%02X%02X&%02X", DX_PING, series, dxMsgSize, id, checksum);

	//Answer: STX 01 SS 01 ER ED CS
	if (!transmitter->txRx(txPacket, rxPacket, 5*2+2))
		printError(ERR_TIMEOUT);

	int receivedDataBytes = getDataFromRxPacket();
	if ( receivedDataBytes == 1)
	{
#if DEBUG_PRINTS
		fprintf(stderr,"Error byte value: %d\n",rxDataBytes[0]);
#endif
		if (!timedOut(rxDataBytes[0]))
			return true;
	}
	else
	{
		fprintf(stderr,"DxComm: Invalid data in packet received from instruction PING\n");
	}

/*	else if ( receivedDataBytes == 2 )
	{
		fprintf(stderr,"Error byte value: %d\n",rxDataBytes[0]);
		fprintf(stderr,"Value read: %d\n",rxDataBytes[1]);
		if (!hasError(rxDataBytes[0]))
			return true;
	}
	else if ( receivedDataBytes == 3 )
	{
		fprintf(stderr,"Error byte value: %d\n",rxDataBytes[0]);
		int receivedWord = ((int)rxDataBytes[2] << 8) | ((int)rxDataBytes[1] & 0xFF);
		fprintf(stderr,"Value read: %d\n",receivedWord);
		if (!hasError(rxDataBytes[0]))
			return true;
	}*/

	return false;
}

void DxComm::dxl_action(int series){
	//Command: STX 02 SS 00 ED CS
	int dxMsgSize = 0;
	unsigned char dataLength = (DX_ACTION + series + dxMsgSize);
	unsigned char checksum = (~dataLength + 1) & 0xff;

	sprintf(txPacket,"%%" "%02X%02X%02X&%02X", DX_ACTION, series, dxMsgSize, checksum);

	//Answer: STX 02 SS 00 ED CS
	if (!transmitter->txRx(txPacket, rxPacket, 4*2+2))
		printError(ERR_TIMEOUT);
}

int DxComm::dxl_read_byte(int series, int id, int address){
	//Command: STX 03 SS 02 ID AA ED CS
	int dxMsgSize = 2;
	unsigned char dataLength = (DX_READ_BYTE + series + dxMsgSize + id + address);
	unsigned char checksum = (~dataLength + 1) & 0xff;

	sprintf(txPacket,"%%" "%02X%02X%02X%02X%02X&%02X", DX_READ_BYTE, series, dxMsgSize, id, address, checksum);

	//Answer: STX 03 SS 02 ER BB ED CS
	if (!transmitter->txRx(txPacket, rxPacket, 6*2+2)) {
		printError(ERR_TIMEOUT);
		return -1;
	}

	int receivedDataBytes = getDataFromRxPacket();
	if ( receivedDataBytes == 2 )
	{
#if DEBUG_PRINTS
		fprintf(stderr,"Error byte value: %d\n",rxDataBytes[0]);
		fprintf(stderr,"Value read: %d\n",rxDataBytes[1]);
#endif
		if (!timedOut(rxDataBytes[0]))
			return rxDataBytes[1];
	}
	else
	{
		fprintf(stderr,"DxComm: Invalid data in packet received from instruction READ_BYTE\n");
	}

	return -1;
}

void DxComm::dxl_write_byte(int series, int id, int address, int value){
	//Command: STX 04 SS 03 ID AA VV ED CS
	int dxMsgSize = 3;
	unsigned char dataLength = (DX_WRITE_BYTE + series + dxMsgSize + id + address + value);
	unsigned char checksum = (~dataLength + 1) & 0xff;

	sprintf(txPacket,"%%" "%02X%02X%02X%02X%02X%02X&%02X", DX_WRITE_BYTE, series, dxMsgSize, id, address, value, checksum);

	//Answer: STX 04 SS 01 ER ED CS |-or-| STX 04 SS 00 ED CS
	if (!transmitter->txRx(txPacket, rxPacket, 5*2+2))
		printError(ERR_TIMEOUT);
}

int DxComm::dxl_read_word(int series, int id, int address){
	//Command: STX 05 SS 02 ID AA ED CS
	int dxMsgSize = 2;
	unsigned char dataLength = (DX_READ_WORD + series + dxMsgSize + id + address);
	unsigned char checksum = (~dataLength + 1) & 0xff;

	sprintf(txPacket,"%%" "%02X%02X%02X%02X%02X&%02X", DX_READ_WORD, series, dxMsgSize, id, address, checksum);

	//Answer: STX 05 SS 03 ER WL WH ED CS
	if (!transmitter->txRx(txPacket, rxPacket, 7*2+2)) {
		printError(ERR_TIMEOUT);
		return -1;
	}

	int receivedDataBytes = getDataFromRxPacket();
	if ( receivedDataBytes == 3 )
	{
		int receivedWord = ((int)rxDataBytes[2] << 8) | ((int)rxDataBytes[1] & 0xFF);
#if DEBUG_PRINTS
		fprintf(stderr,"Error byte value: %d\n",rxDataBytes[0]);
		fprintf(stderr,"Value read: %d\n",receivedWord);
#endif
		if (!timedOut(rxDataBytes[0]))
			return receivedWord;
	}
	else
	{
		fprintf(stderr,"DxComm: Invalid data in packet received from instruction READ_WORD\n");
	}

	return -1;
}

void DxComm::dxl_write_word(int series, int id, int address, int value){
	//Command: STX 06 SS 04 ID AA VL VH ED CS
	int dxMsgSize = 4;
	unsigned char valueLow = (unsigned char)value;
	unsigned char valueHigh = (unsigned char)(value >> 8);
	unsigned char dataLength = (DX_WRITE_WORD + series + dxMsgSize + id + address + valueLow + valueHigh);
	unsigned char checksum = (~dataLength + 1) & 0xff;

	sprintf(txPacket,"%%" "%02X%02X%02X%02X%02X%02X%02X&%02X", DX_WRITE_WORD, series, dxMsgSize, id, address, valueLow, valueHigh, checksum);

	//Answer: STX 06 SS 01 ER ED CS |-or-| STX 06 SS 00 ED CS
	if (!transmitter->txRx(txPacket, rxPacket, 5*2+2))
		printError(ERR_TIMEOUT);
}

void DxComm::dxl_syncWrite_byte(int series, int nServos, int initialAddress, int *idArray, int *valuesArray){
	//Command: STX 07 SS NN AA ID1 VV1 ID2 VV2 ... ED CS
	int dxMsgSize = 2 * nServos + 1; //2 bytes per each servo (id and value) plus the address byte

	sprintf(txPacket,"%%" "%02X%02X%02X%02X", DX_SYNC_WRITE_BYTE, series, dxMsgSize, initialAddress);

	unsigned char dataLength = (DX_SYNC_WRITE_BYTE + series + dxMsgSize + initialAddress);

	int msgIdx = 9; //5 bytes are already written, which occupy twice characters
	int i;
	for (i=0; i < nServos; i++) {
		sprintf(txPacket+msgIdx,"%02X",idArray[i]);
		unsigned char curValue = (unsigned char)valuesArray[i];
		sprintf(txPacket+msgIdx+2,"%02X",curValue);
		dataLength += idArray[i] + curValue; //sum of the elements for checksum calculation
		msgIdx += 4;
	}

	unsigned char checksum = (~dataLength + 1) & 0xff;
	sprintf(txPacket+msgIdx,"&%02X",checksum);

	//Answer: STX 07 SS 00 ED CS
	if (!transmitter->txRx(txPacket, rxPacket, 4*2+2))
		printError(ERR_TIMEOUT);
}

void DxComm::dxl_syncWrite_word(int series, int nServos, int initialAddress, int *idArray, int *valuesArray){
	//Command: STX 08 SS NN AA ID1 VL1 VH1 ID2 VL2 VH2 ... ED CS
	int dxMsgSize = 3 * nServos + 1; //3 bytes per each servo (id, valueLow and valueHigh) plus the address byte

	sprintf(txPacket,"%%" "%02X%02X%02X%02X", DX_SYNC_WRITE_WORD, series, dxMsgSize, initialAddress);

	unsigned char dataLength = (DX_SYNC_WRITE_WORD + series + dxMsgSize + initialAddress);

	int msgIdx = 9; //5 bytes are already written, which occupy twice characters
	int i;
	for (i=0; i < nServos; i++) {
		sprintf(txPacket+msgIdx,"%02X",idArray[i]);
		unsigned char curValueLow = (unsigned char)valuesArray[i];
		unsigned char curValueHigh = (unsigned char)(valuesArray[i] >> 8);
		sprintf(txPacket+msgIdx+2,"%02X%02X",curValueLow,curValueHigh);
		dataLength += idArray[i] + curValueLow + curValueHigh; //sum of the elements for checksum calculation
		msgIdx += 6;
	}

	unsigned char checksum = (~dataLength + 1) & 0xff;
	sprintf(txPacket+msgIdx,"&%02X",checksum);

	//Answer: STX 08 SS 00 ED CS
	if (!transmitter->txRx(txPacket, rxPacket, 4*2+2))
		printError(ERR_TIMEOUT);
}

void DxComm::dxl_regWrite_byte(int series, int id, int address, int value){
	//Command: STX 09 SS 03 ID AA VV ED CS
	int dxMsgSize = 3;
	unsigned char dataLength = (DX_REG_WRITE_BYTE + series + dxMsgSize + id + address + value);
	unsigned char checksum = (~dataLength + 1) & 0xff;

	sprintf(txPacket,"%%" "%02X%02X%02X%02X%02X%02X&%02X", DX_REG_WRITE_BYTE, series, dxMsgSize, id, address, value, checksum);

	//Answer: STX 09 SS 01 ER ED CS |-or-| STX 09 SS 00 ED CS
	if (!transmitter->txRx(txPacket, rxPacket, 5*2+2))
		printError(ERR_TIMEOUT);
}

void DxComm::dxl_regWrite_word(int series, int id, int address, int value){
	//Command: STX 0A SS 04 ID AA VL VH ED CS
	int dxMsgSize = 4;
	unsigned char valueLow = (unsigned char)value;
	unsigned char valueHigh = (unsigned char)(value >> 8);
	unsigned char dataLength = (DX_REG_WRITE_WORD + series + dxMsgSize + id + address + valueLow + valueHigh);
	unsigned char checksum = (~dataLength + 1) & 0xff;

	sprintf(txPacket,"%%" "%02X%02X%02X%02X%02X%02X%02X&%02X", DX_REG_WRITE_WORD, series, dxMsgSize, id, address, valueLow, valueHigh, checksum);

	//Answer: STX 0A SS 01 ER ED CS |-or-| STX 0A SS 00 ED CS
	if (!transmitter->txRx(txPacket, rxPacket, 5*2+2))
		printError(ERR_TIMEOUT);
}

void DxComm::gotoPosition(int series, int id, int position, int speed){
	//Command: STX 10 SS 05 ID PL PH SL SH ED CS
	int dxMsgSize = 5;
	int posLow = (unsigned char)position;
	int posHigh = (unsigned char)(position >> 8);
	int speedLow = (unsigned char)speed;
	int speedHigh = (unsigned char)(speed >> 8);
	unsigned char dataLength = (DX_GOTO_POSITION + series + dxMsgSize + id + posLow + posHigh + speedLow + speedHigh);
	unsigned char checksum = (~dataLength + 1) & 0xff;

	sprintf(txPacket,"%%" "%02X%02X%02X%02X%02X%02X%02X%02X&%02X", DX_GOTO_POSITION, series, dxMsgSize, id, posLow, posHigh, speedLow, speedHigh, checksum);

	//Answer: STX 10 SS 01 ER ED CS |-or-| STX 10 SS 00 ED CS
	if (!transmitter->txRx(txPacket, rxPacket, 5*2+2))
		printError(ERR_TIMEOUT);
}

void DxComm::writePosition(int series, int id, int position, int speed){
	//Command: STX 11 SS 05 ID PL PH SL SH ED CS
	int dxMsgSize = 5;
	int posLow = (unsigned char)position;
	int posHigh = (unsigned char)(position >> 8);
	int speedLow = (unsigned char)speed;
	int speedHigh = (unsigned char)(speed >> 8);
	unsigned char dataLength = (DX_WRITE_POSITION + series + dxMsgSize + id + posLow + posHigh + speedLow + speedHigh);
	unsigned char checksum = (~dataLength + 1) & 0xff;

	sprintf(txPacket,"%%" "%02X%02X%02X%02X%02X%02X%02X%02X&%02X", DX_WRITE_POSITION, series, dxMsgSize, id, posLow, posHigh, speedLow, speedHigh, checksum);

	//Answer: STX 11 SS 01 ER ED CS |-or-| STX 11 SS 00 ED CS
	if (!transmitter->txRx(txPacket, rxPacket, 5*2+2))
		printError(ERR_TIMEOUT);
}

int DxComm::readPosition(int series, int id){
	//Command: STX 12 SS 01 ID ED CS
	int dxMsgSize = 1;
	unsigned char dataLength = (DX_READ_POSITION + series + dxMsgSize + id);
	unsigned char checksum = (~dataLength + 1) & 0xff;

	sprintf(txPacket,"%%" "%02X%02X%02X%02X&%02X", DX_READ_POSITION, series, dxMsgSize, id, checksum);

	//Answer: STX 12 SS 03 ER PL PH ED CS
	if (!transmitter->txRx(txPacket, rxPacket, 7*2+2)) {
		printError(ERR_TIMEOUT);
		return -1;
	}

	int receivedDataBytes = getDataFromRxPacket();
	if ( receivedDataBytes == 3 )
	{
		int receivedWord = ((int)rxDataBytes[2] << 8) | ((int)rxDataBytes[1] & 0xFF);
#if DEBUG_PRINTS
		fprintf(stderr,"Error byte value: %d\n",rxDataBytes[0]);
		fprintf(stderr,"Value read: %d\n",receivedWord);
#endif
		if (!timedOut(rxDataBytes[0]))
			return receivedWord;
	}
	else
	{
		fprintf(stderr,"DxComm: Invalid data in packet received from instruction READ_POSITION\n");
	}

	return -1;
}

void DxComm::printError(ErrType err){
	fprintf(stderr,"DxComm Error: ");
	switch (err){
	case ERR_TIMEOUT:
		fprintf(stderr,"Timeout on GATEWAY communication line!\n");
		break;
	}
}

bool DxComm::timedOut(char errByte)
{
	if(errByte & 0x80)
		return true;

	return false;
}

bool DxComm::hasError(char errByte)
{
	bool retValue = false;
	if(errByte & 0x80)
	{
		fprintf(stderr,"DxComm -> Error on returned packet: Device timeout\n");
		retValue = true;
	}
	if(errByte & 0x40)
	{
		fprintf(stderr,"DxComm -> Error on returned packet: Instruction error\n");
		retValue = true;
	}
	if(errByte & 0x20)
	{
		fprintf(stderr,"DxComm -> Error on returned packet: Overload error\n");
		retValue = true;
	}
	if(errByte & 0x10)
	{
		fprintf(stderr,"DxComm -> Error on returned packet: Checksum error\n");
		retValue = true;
	}
	if(errByte & 0x08)
	{
		fprintf(stderr,"DxComm -> Error on returned packet: Range error\n");
		retValue = true;
	}
	if(errByte & 0x04)
	{
		fprintf(stderr,"DxComm -> Error on returned packet: Overheating error\n");
		retValue = true;
	}
	if(errByte & 0x02)
	{
		fprintf(stderr,"DxComm -> Error on returned packet: Angle limit error\n");
		retValue = true;
	}
	if(errByte & 0x01)
	{
		fprintf(stderr,"DxComm -> Error on returned packet: Input voltage error\n");
		retValue = true;
	}

	return retValue;
}

int DxComm::getDataFromRxPacket()
{
	enum rxStates {IDLE=0, COMM, SERIES, N_BYTES, DATA, CS, ENDFRAME};

	unsigned char checkSum, rxState, rxCount;
	char rxChar, rxCharBin, validFrameReceived;
	int command, devSeries, nrBytesInFrame;

	unsigned int charIdx = 0;

	validFrameReceived = false;
	rxState = IDLE;
	checkSum = rxCount = command = devSeries = nrBytesInFrame = 0;

#if DEBUG_PRINTS
	fprintf(stderr,"RECEIVED PACKET IS: %s\n",rxPacket);
#endif

	rxChar = rxPacket[charIdx];
	while ( rxChar != '\0')
	{
		if(rxChar == SOFR)
		{
			checkSum = command = devSeries = nrBytesInFrame = 0;
			rxCount = 2;		// Number of characters to be read in the next state
			rxState = COMM;
		}
		else if(rxState >= COMM)
		{
			ascii2bin(rxChar, rxCharBin);
			switch(rxState)
			{
				case COMM:
					command |= ((unsigned int)rxCharBin) << ((rxCount-1) * 4);
					if(--rxCount == 0)
					{
						checkSum = command;
						rxCount = 2;
						rxState = SERIES;
					}
					break;

				case SERIES:
					devSeries |= ((unsigned int)rxCharBin) << ((rxCount-1) * 4);
					if(--rxCount == 0)
					{
						checkSum += devSeries;
						rxCount = 2;
						rxState = N_BYTES;
					}
					break;

				case N_BYTES:
					nrBytesInFrame |= (rxCharBin) << ((rxCount-1) * 4);
					if(--rxCount == 0)
					{
						checkSum += nrBytesInFrame;
						rxCount = 0;
						rxState = DATA;
					}
					break;

				case DATA:
					if((rxChar != EODT) && rxCount < (2 * nrBytesInFrame))
					{
						if(rxCount & 0x01)
						{
							rxDataBytes[rxCount >> 1] |= rxCharBin;		// 4 LSBits
							checkSum += rxDataBytes[rxCount >> 1];
						}
						else
							rxDataBytes[rxCount >> 1] = rxCharBin << 4;	// 4 MSBits
						rxCount++;
					}
					else
					{
						if(rxChar != EODT)
						{
							validFrameReceived = false;
							rxState = IDLE;
						}
						else
							rxState = CS;
						rxCount = 2;
					}
					break;

				case CS:
					checkSum += rxCharBin << ((rxCount-1) * 4);

					if(--rxCount == 0)
					{
						if(checkSum == 0)
						{
							validFrameReceived = true;
						}
						else
						{
							fprintf(stderr,"Checksum failed\n");
							validFrameReceived = false;
						}
						rxState = IDLE;
					}
					break;
#if 0
				case ENDFRAME:
					rxState = IDLE;
					break;
#endif
				default:
					rxState = IDLE;
					break;
			}
		}

		//Get next char from the packet
		charIdx++;
		rxChar = rxPacket[charIdx];
	}

#if DEBUG_PRINTS
	fprintf(stderr,"Valid frame: %d, nrBytes: %d\n",validFrameReceived,nrBytesInFrame);
#endif
	if (!validFrameReceived)
		return -1;

	return nrBytesInFrame;
}
